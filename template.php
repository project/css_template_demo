<?php

/**
 * Override or insert PHPTemplate variables into the templates.
 */
function phptemplate_preprocess_page(&$vars) {
  // Hook into css_template.module
  if (module_exists('css_template')) {
    _css_template_page_alter($vars);
  }
}

